/* Writing Comment in JavaScript */
// one line comments (CMD + /)
/* multi-line comments (option + CMD + /) */


console.log("Hello Batch 144");

/*
	Javascript - we can see or log messages in our console.

	Browser Consoles are part of our browsers which will allow us to see/log messages, data or information from our programming language - JavaScript.

	For most browsers, consoles are easily accessed through its developer tools in the console tab.

	In fact, consoles in browsers wil allow us to add some Javascript expressions.

	Statements

		Statements are instructions, expressions that we add to our programming language to communicate with our computers.

		JS Statements usually end in a semi-colon (;), however, JS has implemented a way to acutomatically add semicolons at the end of a line/statement. Semi-colons can actually be omitted in creating JS Statements but semi-colons in JS are added to mark the end of the statement.

	Syntax

		Syntax in programming is a set of rules that describes how statements are properly made/constructed.

		Lines/block of code must follow a certain of rules for it to work properly.
	
*/

console.log("Ralph");



let food1 = "Daing na Bangus";

console.log(food1);

/*
	Variables are a way to store information or data within our JS.

	To create a variable we first declare the name of the variable with either the let/const keyboard:

		let <nameOfVariable>

	Then, we can initialize the variable with a value or data.

		let <nameOfVariable> = <data>;

*/

console.log("My favorite food is: " + food1)

let food2 = "Adobo";

console.log("My favorite food are: " + food1 + " and " + food2)

let food3;

/*
	We can create variables without adding an initial value, however, the variable's content is undefined.

*/

console.log(food3);

food3 = "Chickenjoy";

/* 
	We can update the content of a let variable by reassigning the content using an assignment operator (=) 

	assignment operator (=) lets us assign data to a variable.

		<variable name> = <data>;

	We cannot create another variable with the same name. It will result in an error.

*/

console.log(food3);

food1 = "Sisig";
food2 = "Pizza";

console.log(food1);
console.log(food2);

// const keyword
/*
	const keyword will also allow us to create variables. However, with a const keyword we create constant variables, which means these data to saved in a constant will not change, cannot be changed and should not be changed.

		- trying to re-assign a const variable will result to an error
		- trying to create a const variable without initialization or an initial value will result to an error


*/

const pi = 3.1416;

console.log(pi);

let myName;
const sunriseDirection = "East";
const sunsetDirection = "West";

// you can log multiple items, variables, data BUT separated by commas (,)
console.log(myName, sunriseDirection, sunsetDirection);

/*
	Guides in createing a JS variable:

	1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared.

	2. Creating a variable has two parts: Declaration of the variable name and Initialization of the initial value of the variable using an assignment operator (=) 

	3. A let variable can be declared without initialization. However the value of the variable will be undefined until it is re-assigned with a value.

	4. Not Defined VS Undefined. NOT DEFINED ERROR means the variable is used but NOT declared. UNDEFINED ERROR results from a variable that is used but is not initialized.

	5. We can use const Keyword to create constant variables. Constant variables cannot be declared without initialization. Constant variables cannot be re-assigned.

	6. When creating variable names, start with small caps, this is because of avoiding conflict with syntax in JS that is named with capital letters.

	7. If the varible would need two words, the naming convention is the use of CAMEL CASE. Do not add a space for variables with words as names.

*/


// DATA TYPES

/* ######### STRINGS 

	STRINGS - are data which are alphanumerical text. It could be a name, a phrase or even a sentence. We can create string with sing quote('') or with double quote ("")

	<variable> = "<string>"
	
*/

console.log("Sample Sting Data");

let country = "Philippines";
let province = "Rizal";

console.log(province, country);

/*
	CONCATENATION - is a way for us to add strings together and have a single string. We can use "+" symbol for this. 

		let <variable> = <variable1> + ',' + <variable2>....;

*/

let fullAddress = province + ', ' + country;

console.log(fullAddress);


let greeting = "I live in " + country;

console.log(greeting);

/* In JS, when you use the + sign with strings we have concatenation */


let numString = "50";
let numString2 = "25";

console.log(numString + numString2);

/*
	Strings have Property called .length and it tells us the number of charaters in a string.

	Spaces, commas, Periods can also be characters when included in a string.

	It will return a NUMBER-type data.

*/

let hero = "Captain America";

console.log(hero.length);

/* #########  NUMBERS #######

	NUMBER-type data can actually be used in proper mathematical equations and opertations. 

*/

let students = 16;
console.log(students);

let num1 = 50;
let num2 = 25;
console.log(num1 + num2);

/* 
	Additional Operator will allow us to add two number types and return the sum as a data type. 

	Operations return a value and that value can be saved in a variable.

	What if we use the addition operator on a number type and a numerical string? It results to concatenation.
*/

let sum1 = num1 + num2;

console.log(sum1);

let numString3 = "100";

let sum2 = parseInt(numString3) + num1

console.log(sum2);

// Mini-Activity

let sum3 = sum1 + sum2;
let sum4 = parseInt(numString2) + num2;

console.log(sum3, sum4);

// End of Activity


/*
	Subraction operator will let us get the difference between the two number types. It will also result to a proper number type data.

	WHEN Subtraction operator is used on a string and a number, the string will be converted into a number automatically and then JS will perform the operation. 

	this Automatic conversion from one type to another is called:
		TYPE CONVERSION or TYPE COERSION or FORCED COERSION

	When a TEXT STRING is subtracted with a number, it will result in NaN or NOT A NUMBER because when JS converted the test string it results to NaN and NaN-Number = NanN

*/

let difference = num1 - num2;
console.log(difference);

let difference2 = (numString3 - num2);
console.log(difference2);

let difference3 = hero - num2;
console.log(difference3);

/*
	Multiplication operator (*)
*/

let num3 = 10;
let num4 = 5;
let product = num3 * num4;
console.log(product);

let product2 = numString3 * num3;
console.log(product2);

let product3 = numString * numString3;
console.log(product3);



/*
	Division operator (*)
*/

let num5 = 30;
let num6 = 3;
let quotient = num5 / num6;
console.log(quotient);

let quotient2 = (numString3 / 5);
console.log(quotient2);

let quotient3 = (450 / num4);
console.log(quotient3);

/* 
	Boolean (true / false)
		- usually used for logical operations or for if-else  conditions.
		- naming convention for a variable that contains boolean is a YES or NO question

*/

let isAdmin = true;
let isMarried = false;

/* 
	You can actually name uyour variable any way you want however as the best practice it should be:

		1. Appropriate
		2. Definitive of the value it contains
		3. Semantically correct

		bad naming:
			let pikachu = "Tee Jae Bengan Calinao"
			console.log(pikachu)
*/

/*
	ARRAYS	
		- are a special kind of data wherein we can store multiple values. 
		- an array can store multiple values and even of different types. For best practice, keep the data type of items in an array uniform.
		- best practice is to have a the same data types in an array
		- VALUES in an array are separated by comma. Failing to do so, will result in an error. 
		- an Array is created with an Array Literal []

			let <variable> = [name1, name2, ..., nameX]
			index				0 	   1           n

		array indices are markers of the order of the items in the array. Array index starts at ZERO 0

		To access an array item:
			arrayName[index]

*/

let koponanNiEugene = ["Eugene", "Alfred", "Dennis", "Vincent"]
console.log(koponanNiEugene);

// to get the items in an array we use their index number
console.log(koponanNiEugene[0]);
// Bad Practice
let array2 = ["One Punch Man", true, 500, "Saitama"];
console.log(array2);


/* 
	OBJECTS
		- are another kind of special data type used to mimic real world objects.
		- with this we can add niformation that makes sense, thematically relevent and of different data types.

		Objects can be created with Objects Literals {}

		Each value is given a label which makes the data significant and meaningful this pairing and label is what we call a KEY:VALUE PAIR
			A key-value pair together is called a PROPERTY.
				- and each property is separated by a comma.
				- properties of an object should relate to each other and thematically relevant to describe a single item/object


*/

let person1 = {

	 /* key  :  value pair */
	heroName: "One Punch Man", /* Property */
	isRegistered: true,
	salary: 500,
	realName: "Saitama"

}

// To Access an Object's Property
console.log(person1.realName);

// Mini-Activity

let myFavoriteBands = ["Ben&Ben", "Parokya ni Edgar", "Orange & Lemons"]

let me = {

	firstName: "Ralph",
	lastName: "Dequina",
	isWebDeveloper: true,
	hasPortfolio: true,
	age: 26

}

console.log(myFavoriteBands);
console.log(me)



/*
	UNDEFINED vs NULL

	Null	- is the explicit declaration that there is no value.
			- "as in wala. wala sya."
			- like "express nothing"

*/

		let sampleNull = null;

/*

	Undefined	- means that the variable exists, however a value
				  was not initialized with the variable

*/


		let undefinedSample;
		console.log(undefinedSample);


// Certain processes in programming explicitly returns null to indicate that the task resulted to nothing.
	

		let foundResult = null;

// For undefined, this is normally caused by developers creating variables that have no value or data associated with them.
// The variable DOES exist but its value is still UNKNOWN.
let person2 = {
	name: "Patricia",
	age: 28
}

// this will work because the variable person2 DOES exist, however the property isAdmin does NOT 
console.log(person2.isAdmin);























