// Functions
/*
	- Functions are used to store a line of codes that you want to be reused is multiple times on your program
	- it is like a "program" within a program

*/

function printStar() {
	console.log("*");
	console.log("**");
	console.log("***");
}

// printStar();

function sayHello() {
	console.log('Hello');
}
// sayHello();


// the arrangement of the code matters from top to bottom
function alertPrint() {
	alert("Hello"); // alert is use to have a pop-up alert window
	sayHello(); // a function can call a function outside
}

// alertPrint();

// 2 PARAMETERS Function

/* num1, num2 are called PARAMETERS; an additional information need to a funciton to work*/
function addSum(num1,num2) { 
	let sum = num1 + num2;	
	console.log(sum);
}

/* ARGUMENTS - is the actual values of the parameters (e.g. (13,2) ß*/
// addSum(13,2); 
// addSum(24,56);


function sayHello2(name){
	console.log("Hello " + name);
}

// string must be supplied to the funct if not, it won't work
// sayHello2('Zuitt'); 
// because of Coersion, JS would convert 6 to a string type
// sayHello2(6);

// 3 PARAMETER Function

function printBio(lname,fname,age) {
	// console.log('Hello ' + lname + " " + fname + age);
	console.log(`Hello Mr. ${lname} ${fname} ${age}`) 
	// this is Interpolation or String Template Literals
	// and this is the newer best practice nowadays
}

// printBio('Start', 'Tony', 50)



// RETURN KEYWORD

function addSum2(x,y) { 
	console.log(x+y)
	return y-x;
	/* used to return a particular value from the function to the function called. You may call the function using the function name and typing it on Chrome Console

		NOTE: the "return func" is the end of the function, thus, any line of code under it won't be read by JS

		example:
			function addSum2(x,y) { 
				return y-x;
				console.log(x+y)
			}
	*/ 
}	

// let sumNum = addSum2(3,4);

















