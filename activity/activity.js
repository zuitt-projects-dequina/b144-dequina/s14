// Activity

function printBio(lname,fname,email,mobileNum) {
	console.log(`Hello! I am ${lname} ${fname}.`);
	console.log(`You can contact me via ${email}`);
	console.log(`Mobile Number: ${mobileNum}`);
}

printBio('Rogers','Steve','capt_am@gmail.com','09171234567');